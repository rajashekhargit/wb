import React, { Component ,useState,useEffect} from "react";
import Konva from "konva";
import { Group, Image, Rect,Stage,Layer } from "react-konva";

 class Ani extends Component {
    constructor(props) {
        super(props);

        this.video = document.createElement("video");
        this.video.addEventListener(
            "loadedmetadata",
            // this.updateImageSize.bind(this)
            console.log("hai")
        );
        this.video.src = this.props.src;
    }

    // updateImageSize() {
    //     const { image, video } = this;
        //
        // // const newImageHeight =
        // //     (image.width() * video.videoHeight) / video.videoWidth;
        // const currentImagePosition = image.position();
        //
        // image.position({
        //     x: currentImagePosition.x,
        //     y: currentImagePosition.y + (image.height() - newImageHeight) / 2
        // });
        // image.height(newImageHeight);
    // }

    play() {
        this.video.play();
        this.animation.start();
    }

    pause() {
        this.video.pause();
        this.animation.stop();
    }

    componentDidMount() {
        const { state } = this.props;

        this.animation = new Konva.Animation(() => {}, this.image.getLayer());

        if (state === "play") {
            this.play();
        }
    }

    componentDidUpdate() {
        const { state } = this.props;

        // this.updateImageSize();

        switch (state) {
            case "play":
                return this.play();
            case "pause":
                return this.pause();
            default:
                throw new Error("Unxknown state: " + state);
        }
    }

    componentWillUnmount() {
        this.pause();
    }

    render() {
        const { video } = this;
        const { x, y, width, height } = this.props;

        return (
            <Group x={x} y={y} width={width} height={height}>
                <Image
                    x={0}
                    y={0}
                    width={width}
                    height={height}
                    image={video}
                    ref={node => {
                        this.image = node;
                    }}
                />
            </Group>
        );
    }
}


class Anim extends Component {
    state = {
        state: "pause",
        width: 300
    };

    handleStateChanged = event => {
        this.setState({ state: event.target.value });
    };

    handleWidthChanged = event => {
        this.setState({ width: parseInt(event.target.value, 10) });
    };

    render() {
        const { state, width } = this.state;

        return (
            <div>
                <select value={state} onChange={this.handleStateChanged}>
                    <option value="pause">Pause</option>
                    <option value="play">Play</option>
                </select>

                <select value={width} onChange={this.handleWidthChanged}>
                    <option value="100">100</option>
                    <option value="300">300</option>
                    <option value="500">500</option>
                </select>

                <Stage width={window.innerWidth - 20} height={window.innerHeight - 50}>
                    <Layer>
                        <Ani
                            x={10}
                            y={10}
                            width={width}
                            height={300}
                            src= 'https://upload.wikimedia.org/wikipedia/commons/transcoded/c/c4/Physicsworks.ogv/Physicsworks.ogv.240p.vp9.webm'

                        state={state}
                        />
                    </Layer>
                </Stage>
            </div>
        );
    }
}

export default Anim;
