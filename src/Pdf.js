import React from "react";
import Konva from "konva";
import {Stage, Layer, Line,Image} from 'react-konva';
var PDFImage = require("pdf-image").PDFImage;

const Pdf = () =>{
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const[size,setSize] = React.useState(4);
    const[color,setColor]= React.useState("red");
    const [stageScale,setstageScale] = React.useState(1);
    const [stageX,setstageX] =React.useState(0);
    const [drag,serDrag] = React.useState(false);
    const [stageY,setstageY] =React.useState(0);
    const [pageNum,setPageNum] = React.useState(0);
    const style = {
        backgroundColor:'white',
        cursor : 'crosshair'
    };
    let url;
let img;
let renderPage;
    const doc= (e) => {

        var file = e.target.files[0];

        var pdfjsLib = window['pdfjs-dist/build/pdf'];
        let fileReader = new FileReader();

        fileReader.onload = function () {
            // console.log(fileReader.result);

            let typedarray = new Uint8Array(fileReader.result);
//         pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
            let pdfDoc = null,
                pageNum = 1,
                pageRendering = false,
                pageNumPending = null,
                scale = 0.8;
            let canvas = document.getElementsByTagName('canvas')[0];
            let ctx = canvas.getContext('2d');
            // let URL = window.webkitURL || window.URL;
            // url = URL.createObjectURL(e.target.files[0]);
            // let pdfjsLib = window['pdfjs-dist/build/pdf'];
            // pdfjsLib.GlobalWorkerOptions.workerSrc = '';
            // let loadingTask = pdfjsLib.getDocument(url);
            // loadingTask.promise.then(function (pdf) {
            //     console.log('PDF loaded');
            //     let pageNumber = 1;
            //     pdf.getPage(pageNumber).then(function (page) {
            //         console.log('Page loaded');
            //
            //         let scale = 1;
            //         console.log(page)
            //         let viewport = page.getViewport({scale: scale});
            //         console.log(viewport.height);
            //         let canvas = document.getElementsByTagName('canvas')[0];
            //         let context = canvas.getContext('2d');
            //         canvas.height = viewport.height *4 ;
            //         canvas.width = viewport.width*4;
            //         console.log(canvas.height)
            //         let renderContext = {
            //             canvasContext: context,
            //             background: 'rgba(0,0,0,0)',
            //             viewport: viewport
            //         };
            //         console.log(renderContext)
            //         let renderTask = page.render(renderContext);
            //         console.log(renderTask);
            //         renderTask.promise.then(function () {
            //             console.log('Page rendered');
            //         });
            //     });
            // }, function (reason) {
            //     // PDF loading error
            //     console.error(reason);
            // });
             renderPage = (num) => {
                pageRendering = true;
                // Using promise to fetch the page
                pdfDoc.getPage(num).then(function (page) {
                    var viewport = page.getViewport(1);
                    // canvas.height = 668;
                    // canvas.width = 548;
                    var renderContext = {
                        canvasContext: ctx,
                        viewport: viewport
                    };
                    var renderTask = page.render(renderContext);

                    // Wait for rendering to finish
                    renderTask.promise.then(function () {
                        pageRendering = false;
                        if (pageNumPending !== null) {
                            renderPage(pageNumPending);
                            pageNumPending = null;
                        }
                    });
                });
                document.getElementById('page_num').textContent = num;
            }

            function queueRenderPage(num) {
                if (pageRendering) {
                    pageNumPending = num;
                } else {
                    renderPage(num);
                }
            }

            function onPrevPage() {
                if (pageNum <= 1) {
                    return;
                }
                pageNum--;
                queueRenderPage(pageNum);
            }

            document.getElementById('prev').addEventListener('click', onPrevPage);

            function onNextPage() {
                if (pageNum >= pdfDoc.numPages) {
                    return;
                }
                pageNum++;
                queueRenderPage(pageNum);
            }

            document.getElementById('next').addEventListener('click', onNextPage);


            pdfjsLib.getDocument(typedarray).promise.then(function (pdfDoc_) {
                pdfDoc = pdfDoc_;
                document.getElementById('page_count').textContent = pdfDoc.numPages;
                console.log(pdfDoc._currentScale);
                renderPage(pageNum);
            });
        };
        fileReader.readAsArrayBuffer(file);

    };
    const  handleWheel = e => {
        e.evt.preventDefault();

        const scaleBy = 1.01;
        const stage = e.target.getStage();
        const oldScale = stage.scaleX();
        const mousePointTo = {
            x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
            y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
        };

        const newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

        stage.scale({ x: newScale, y: newScale });
        setstageScale(newScale);
        setstageX( -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale);
        setstageY(-(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale);

    };
    // const handleZoomIn  = () => {
    //     let newScale = pdfD.currentScale + DEFAULT_SCALE_SIZE;
    //     pdfViewer.currentScaleValue = newScale;
    // }
    //
    //
    // const handleZoomOut  = () => {
    //     var newScale = pdfViewer.currentScale - DEFAULT_SCALE_SIZE ;
    //     pdfViewer.currentScaleValue = newScale;
    // }
    return(
        <div>
            <button id="prev">Previous</button>
            <button id="next">Next</button>
            <input type="text" value={pageNum} onChange={e=>{
                setPageNum(parseInt(e.target.value))
            }}/>
            <button onClick={e=>{
                renderPage(pageNum)
            }}>Jump To</button>
            {/*<button id='pdfZoominbutton' onClick={handleZoomIn}>zoom in</button>*/}
            {/*<button id='pdfZoomOutbutton' onClick={handleZoomOut}>zoom out</button>*/}
            <span>Page: <span id="page_num"/> / <span id="page_count"/></span>
            <input type="file" accept="*" onChange={doc}/>
            <select
                value={tool}
                onChange={e => {
                    setTool(e.target.value);
                }}
            >
                <option value="pen">Pen</option>
                <option value="eraser">Eraser</option>
            </select>
            <Stage
                // draggable={true}
                scaleX={stageScale}
                scaleY={stageScale}
                x={stageX}
                // visible={false}
                y={stageY}

                // onWheel={handleWheel}
                style={style}
                width={window.outerWidth}
                height={window.outerHeight}
                onMouseDown={e => {
                    serDrag(true);
                    toggleDrawing(true);
                    const pointer = e.target.getStage().getPointerPosition();
                    const newLines = lines.concat({
                        id: Date.now(),
                        tool: tool,
                        points: [pointer.x, pointer.y]
                    });
                    setLines(newLines);
                }}
                onMouseMove={e => {
                    if (isDrawing) {
                        const pointer = e.target.getStage().getPointerPosition();
                        const newLines = lines.slice();
                        const lastLine = {
                            ...newLines[newLines.length - 1]
                        };
                        lastLine.size=size;
                        lastLine.color=color;
                        lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                        newLines[newLines.length - 1] = lastLine;
                        setLines(newLines);

                    }else{
                        return;
                    }

                }}
                onMouseUp={e => {
                    toggleDrawing(false);
                }}
            >
                <Layer>

            {/*<Image  image={}/>*/}


                </Layer>
                <Layer
                    clockwise={false}
                    draggable
                    fill="blue"
                    onDragEnd={e => {
                        e.target.to({
                            duration: 0.5,
                            easing: Konva.Easings.ElasticEaseOut,
                            scaleX: 1,
                            scaleY: 1,
                            shadowOffsetX: 0,
                            shadowOffsetY: 0
                        });
                    }}
                    onDragStart={e => {
                        e.target.setAttrs({
                            shadowOffset: {
                                x: 15,
                                y: 15
                            },
                            scaleX: 1.1,
                            scaleY: 1.1
                        });
                    }}
                >
                    {lines.map(line => (
                        <Line
                            x={window.length}
                            y={window.length}
                            width={window.length}
                            height={window.length}
                            key={line.id}
                            strokeWidth={line.size}
                            stroke={line.color}
                            points={line.points}
                            globalCompositeOperation={
                                line.tool === "eraser" ? "destination-out" : "source-over"
                            }
                        />
                    ))}
                </Layer>

            </Stage>

        </div>
    )


};

export default Pdf;
