import React, {useState} from "react";
import { render } from "react-dom";
import { Stage, Layer, Line,Image,Text,Group,Arc,Ellipse,RegularPolygon,Arrow,Star,Transformer, Rect,Circle } from "react-konva";
import { CirclePicker} from 'react-color';
import Konva from "konva";
import GooglePicker from "react-google-picker";
import {gapi} from "gapi-script";

const Pen = () => {
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const[size,setSize] = React.useState(3);
    const[color,setColor]= React.useState("red");
    const [rectangle,setRect]=React.useState([]);
    const[circle,setCircle] = React.useState([]);
    const[arc,setArc]= React.useState([]);
    const [ellipse,setEllipse] = React.useState([]);
    const[rp,setRp] = React.useState([]);
    const [arrow,setArrow] = React.useState([]);
    const[star,setStar] = React.useState([]);
    const[fill,setFill]= React.useState("");
    const [layer, setLayer] = React.useState([]);
    const [x, setX] = React.useState(250);
    const [y, setY] = React.useState(300);
    const [width, setWidth] = React.useState(100);
    const [height, setHeight] = React.useState(100);
    const [angle, setAngle] = React.useState(100);
    const [shape, setShape] = React.useState("");
    const [selectedNode, setSelectedNode] = React.useState(null);
    const[transformer,setTransformer] = React.useState(null);
    const [stage,setStage] = React.useState(null);
    const [zIndex,setIndex] = React.useState(0);
    const[image,setImage] = React.useState([]);
    let developerKey = 'AIzaSyAWKiW0bm9losRRfYuabxGAzdfH3FBN8KQ';
    let clientId = "711519097060-a2tu4q4oltdi9p60qoog3fkn8ac30nq5.apps.googleusercontent.com"
    let appId = "711519097060";
    let scope = ['https://www.googleapis.com/auth/drive.file'];
    let pickerApiLoaded = false;
    let oauthToken;
    // let transformer;
    // let  selectedNode;

    const handleLayer=() =>{
        toggleDrawing(true);
        setLayer([...layer,{...newLayer()}])
    };
    const handleErase = () =>{
        toggleDrawing(false)
    };
    const newLayer = () => ({
        width: window.innerWidth,
        height: window.innerHeight,
        fill:'black'
    });
    const newRectangle = () => ({
        x: (100),
        y: (200),
        width: (width),
        height: (height),
        fill:'white',
        angle:angle,
        type: "rectangle",
        id : Date.now().toString()
    });
    const newCircle = () =>({
        x: 200,
        y:100,
        radius:50,
        type: "circle"

    });
    const newArc=() =>({
        angle:180,
        innerRadius:120,
        outerRadius:140,
        x:250,
        y:300,
        type: "arc"

    });
    const newEllipse=() =>({
        x: 350,
        y: 350,
        radiusX: 100,
        radiusY: 50,
        type:'ellipse'

    });
    const newRp = ()=>({
        x: 300,
        y: 250,
        sides: 6,
        radius: 70,
        type: "regularPolygon"

    });
    const newArrow = () =>({
        x: 250,
        y: 200,
        points: [0, 0, 350 / 2, 400 / 2],
        pointerLength: 20,
        pointerWidth: 20,
        type: "arrow"

    });
    const newstar = () =>({
        x: 250,
        y: 300,
        numPoints: 6,
        innerRadius: 40,
        outerRadius: 70,
        type: "star"

    });
    const handleArc=() =>{
        setArc([...arc,{...newArc()}])
    };
    const handleEllipse=() =>{
        setEllipse([...ellipse,{...newEllipse()}])
    };
    const handleRP=() =>{
        setRp([...rp,{...newRp()}])
    };
    const handleStar = () =>{
        setStar([...star,{...newstar()}]);
    };
    const handleArrow = () =>{
        setArrow([...arrow,{...newArrow()}]);
    };



    const handleCircle = () => {

        setCircle( [...circle, { ...newCircle() }])
    };
    const handleRect = () => {

            setRect( [...rectangle, { ...newRectangle() }])
    };
    const handleImage=(e) =>{
        let img;
        let URL = window.webkitURL || window.URL;
        console.log(e.target.files[0])
        let url = URL.createObjectURL(e.target.files[0]);
        img = new window.Image();
        img.src = url;
        console.log(img);
        img.onload=()=>{
            setImage([...image, img])
        }
        let x = document.getElementsByTagName('canvas')[0];
        console.log(x)

    };
    const style = {
        backgroundColor:'white',
    cursor : 'crosshair'
    };

    return (
        <div>
            <CirclePicker  value={color} color={color} onChange = {e=>{
                setColor(e.hex);
            }}/>
<button onClick={e=>{
    console.log(selectedNode);
    // selectedNode.dest
    if(selectedNode){
        selectedNode.destroy();
        transformer.detach(selectedNode);
        selectedNode.visible(false);
        setShape('');
        setSelectedNode('');
        let type = selectedNode.attrs.type;
        console.log(type);
        setImage([...image,''])
        switch(type){
            case("rectangle"):{
                setRect([...rectangle,'']);
                break;
            }
            case ("arc"):{
                setArc([...arc,'']);
                break;
            }
            case("ellipse") : {
                setEllipse([...ellipse,'']);
                break;
            }
            case("regularPolygon") : {
                setRp([...rp,'']);
                break;
            }
            case("star") : {
                setStar([...star,'']);
                break;
            }
            case("arrow") : {
                setArrow([...arrow,'']);
                break;
            }
            case("circle") : {
                setCircle([...circle,'']);
                break;
            }



        }
        // setRect([...rectangle,'']);
        // setArc([...arc,''])
        // setEllipse([...ellipse,''])
        // setRp([...rp,'']);
        // setStar([...star,'']);
        // setArrow([...arrow,'']);
        // setCircle( [...circle, '']);
    }

    // transformer.hide();


}}>Remove</button>
            <select
                value={tool}
                onChange={e => {
                    setTool(e.target.value);
                }}
            >
                <option value="pen">Pen</option>
                <option value="eraser">Eraser</option>
            </select>
            <input value={size} onChange={e =>{
                setSize(parseInt(e.target.value))
                console.log(setSize)
            }} type='range' step='3' min='3' max='16'/>
            <button onClick={handleRect}>Add Rectangle</button>
            <button onClick={handleCircle}>Add Circle</button>
            <button onClick={handleArc}>Add Arc</button>
            <button onClick={handleArrow}>Add Arrow</button>
            <button onClick={handleEllipse}>Add Ellipse</button>
            <button onClick={handleRP}>Add Polygon</button>
            <button onClick={handleStar}>Add Star</button>
            <button onClick={handleLayer}>Add pen</button>
            <button onClick={handleErase}>Add erase</button>
            <button onClick={e=>{
                if(selectedNode){
                    selectedNode.attrs.fill=(color);
                    let type = selectedNode.attrs.type;
                    console.log(selectedNode)
                    switch(type){
                        case("rectangle"):{
                            console.log(rectangle);
                            setRect([...rectangle,'']);
                            break;
                        }
                        case("ellipse") : {
                            setEllipse([...ellipse,'']);
                            break;
                        }
                        case("regularPolygon") : {
                            setRp([...rp,'']);
                            break;
                        }
                        case("star") : {
                            setStar([...star,'']);
                            break;
                        }
                        case("arrow") : {
                            setArrow([...arrow,'']);
                            break;
                        }
                        case("circle") : {
                            setCircle([...circle,'']);
                            break;
                        }



                    }
                }
            }}>Fill Color</button>
            <input type="file" onChange={handleImage}/>

            <GooglePicker clientId={clientId}

                          developerKey={developerKey}
                          scope={['https://www.googleapis.com/auth/drive.readonly']}
                          onChange={data => console.log('on change:', data)}
                          onAuthFailed={data => console.log('on auth failed:', data)}
                          multiselect={true}
                          navHidden={true}
                          authImmediate={false}
                          viewId={'FOLDERS'}
                          createPicker={ (google, oauthToken) => {
                              const googleViewId = google.picker.ViewId.FOLDERS;
                              const docsView = new google.picker.DocsView(googleViewId)
                                  .setIncludeFolders(true)
                                  .setMimeTypes("image/png,image/jpeg,image/jpg")
                                  .setSelectFolderEnabled(true);

                              const picker = new window.google.picker.PickerBuilder()
                                  .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                                  .addView(docsView)
                                  .setOAuthToken(oauthToken)
                                  .setDeveloperKey(developerKey)
                                  .setCallback((data)=>{
                                      console.log('Custom picker is ready!',data.docs);
                                      if(data.docs){
                                          let blob;
                                          for(let i = 0; i<data.docs.length;i++){
                                              let obj = data.docs[i];
                                              var accessToken = gapi.auth.getToken().access_token;
                                              console.log(gapi)
                                              console.log(accessToken)
                                              var xhr = new XMLHttpRequest();
                                              var url = "https://www.googleapis.com/drive/v3/files/" + obj.id + "?alt=media";
                                              xhr.open('GET', url);
                                              xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
                                              xhr.responseType = "blob";

                                              xhr.addEventListener('load', function(e) {
                                                  blob = this.response;//this is your blob file
                                                  var file = new File([blob], "file_name", {lastModified: Date.now()});
                                                  console.log(file)
                                                  let img = new window.Image();
                                                  let url = URL.createObjectURL(file);
                                                  img.src = url;
                                                  img.onload=()=>{
                                                      setImage([...image, img])}
                                              });

                                              xhr.send();
                                          }
                                      }
                                  });

                              picker.build().setVisible(true);
                          }}
            >
                <button>Upload from Google drive</button>
                <div className="google"/>
            </GooglePicker>

            <Stage style={style}
                   // onClick={e=>{
                   //     // console.log(e)
                   //     setStage ( transformer.getStage());
                   // }}

                width={window.innerWidth}
                height={window.innerHeight}
                onMouseDown={e => {
                    toggleDrawing(true);
                    const pointer = e.target.getStage().getPointerPosition();
                    const newLines = lines.concat({
                        id: Date.now(),
                        tool: tool,
                        points: [pointer.x, pointer.y]
                    });
                    setLines(newLines);
                }}
                onMouseMove={e => {
                    if (!isDrawing) {
                        return;
                    }
                    const pointer = e.target.getStage().getPointerPosition();
                    const newLines = lines.slice();
                    const lastLine = {
                        ...newLines[newLines.length - 1]
                    };
                    lastLine.size=size;
                    lastLine.color=color;
                    lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                    newLines[newLines.length - 1] = lastLine;
                    setLines(newLines);
                }}
                onMouseUp={e => {
                    toggleDrawing(false);
                }}
            >
                <Layer id="hello">
                    {image.map((image, key) => (
                            <Image

                                onDblClick={e=>{
                                    setStage ( transformer.getStage());
                                    // console.log(stage.findOne("." + e.target.attrs.name))
                                    console.log(e.target.attrs.name);
                                    setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));

                                    if (selectedNode === transformer.node()) {
                                        return;
                                    }
                                    if (selectedNode) {
                                        transformer.attachTo(selectedNode);
                                    } else {
                                        transformer.detach();
                                    }
                                    transformer.show();
                                    transformer.getLayer().batchDraw();
                                }}
                                image={image}
                                key={key}
                                // height={400}
                                // width={}
                                x={190}
                                y={240}
                                stroke={"black"}
                                strokeWidth={2}
                                name={"image" + key}
                                draggable={true}/>

                        ),
                    )}

                </Layer>
                <Layer

                // onClick={e=>{
                //     setStage ( transformer.getStage());
                //     setShape(e.target.name());
                //     console.log(e.target.name(), "iufhuef");
                // }}
                    >
                    <Group>
                    {rectangle.map(({ height, width, x, y , type,id}, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Rect
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name);
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));

                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                console.log(transformer.getStage().find("Rect"))
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.show();
                                transformer.getLayer().batchDraw();
                            }}
                            // onTransform={}
                            onTransformEnd={e=>{
                                const shape = e.target;
                                   setX(shape.x());
                                    setY( shape.y());
                                    setWidth(shape.width() * shape.scaleX());
                                    setHeight(shape.height() * shape.scaleY());
                                    setAngle( shape.rotation());

                            }}
                            onDragStart={e=>{
                                if(selectedNode){
                                    console.log(e)
                                    if(selectedNode.name() === e.target.name())
                                        selectedNode.setZIndex(rectangle.length -1)
                                }
                            }}
                            // onDragEnd={onDragEnd}
                            key={key}
                            x={x}
                            type={type}
                            y={y}
                            width={width}
                            height={height}
                            draggable={selectedNode}
                            fill={fill}
                            stroke="black"
                            id={id}
                            // onDblClick={e=>{
                            //     // setFill(color)
                            // }}
                            name={"rect" +key}
                        />
                    ))}
                    </Group>
                    <Group>
                    {arc.map(({  angle, innerRadius,outerRadius,x,y,type }, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Arc
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)
                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            angle={angle}
                            innerRadius={innerRadius}
                            outerRadius={outerRadius}
                            clockwise={false}
                            x={x}
                            type={type}
                            y={y}
                            draggable={selectedNode}
                            fill="white"
                            name={"arc" +key}
                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                        />
                    ))}
                    </Group>
                    <Group>
                    {circle.map(({  x, y,radius,type }, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Circle
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)

                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            x={x}
                            y={y}
                            type={type}
                            radius={radius}
                            draggable
                            fill={fill}
                            stroke="black"
                            name={"circle" + key}
                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}

                        />
                    ))}
                    </Group>
                <Group>
                    {ellipse.map(({  radiusY,radiusX,x,y,type }, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Ellipse
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)

                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            x={x}
                            y={y}
                            fill={fill}
                            type={type}
                            stroke="black"
                            name={"ellipse" + key}
                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            radiusX={radiusX}
                            radiusY={radiusY}
                            draggable


                        />
                    ))}
                </Group>
                    <Group>
                    {rp.map(({  x, y,sides ,radius,type}, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <RegularPolygon
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)

                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            sides={sides}
                            x={x}
                            y={y}
                            radius={radius}
                            draggable
                            fill={fill}
                            stroke="black"
                            name={"rp" + key}
                            type={type}


                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                        />
                    ))}
                    </Group>
                    <Group>
                    {arrow.map(({  x, y,points,pointerLength ,pointerWidth,type}, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Arrow
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)

                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            x={x}
                            y={y}
                            points={points}
                            pointerLength={pointerLength}
                            pointerWidth={pointerWidth}
                            draggable={true}
                            fill={fill}
                            stroke="black"
                            type={type}

                            // onDblClick={e=>{
                            //     setFill(color)
                            // }}
                            name={"arrow" + key}

                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                        />
                    ))}
                    </Group>
                    <Group>
                    {star.map(({  x, y,numPoints,innerRadius,outerRadius,type }, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                        <Star
                            onDblClick={e=>{
                                setStage ( transformer.getStage());
                                // console.log(stage.findOne("." + e.target.attrs.name))
                                console.log(e.target.attrs.name)
                                setSelectedNode (transformer.getStage().findOne("." + e.target.attrs.name));
                                console.log(selectedNode)
                                if (selectedNode === transformer.node()) {
                                    return;
                                }
                                if (selectedNode) {
                                    transformer.attachTo(selectedNode);
                                } else {
                                    transformer.detach();
                                }
                                transformer.getLayer().batchDraw();
                            }}
                            key={key}
                            x={x}
                            y={y}
                            numPoints={numPoints}
                            innerRadius={innerRadius}
                            outerRadius={outerRadius}
                            draggable
                            fill={fill}
                            name={"star" + key}
                            type={type}


                            stroke="black"
                            // onDblClick={e=>{
                            //     setFill(color)
                            // }}
                            // onTransformEnd={e=>{
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                            // onDragEnd={e => {
                            //     const shape = e.target;
                            //     setX(shape.x());
                            //     setY( shape.y());
                            //     setWidth(shape.width() * shape.scaleX());
                            //     setHeight(shape.height() * shape.scaleY());
                            //     setAngle( shape.rotation());
                            //
                            // }}
                        />

                    ))}
                    </Group>
                    <Transformer
                        // selectedShapeName={shape}
                        ref={node => {
                            setTransformer ( node);
                        }}
                    />
                </Layer>
                <Layer
                    >
                    {lines.map(line => (
                        <Line
                            // draggable={true}
                            x={window.length}
                            y={window.length}
                            width={window.length}
                            height={window.length}
                            onDragEnd={e => {
                                e.target.to({
                                    duration: 0.5,
                                    easing: Konva.Easings.ElasticEaseOut,
                                    scaleX: 1,
                                    scaleY: 1,
                                    shadowOffsetX: 0,
                                    shadowOffsetY: 0
                                });
                            }}
                            onDragStart={e => {
                                e.target.setAttrs({
                                    shadowOffset: {
                                        x: 15,
                                        y: 15
                                    },
                                    scaleX: 1.1,
                                    scaleY: 1.1
                                });
                            }}
                             key={line.id}
                            strokeWidth={line.size}
                            stroke={line.color}
                            points={line.points}
                            globalCompositeOperation={
                                line.tool === "eraser" ? "destination-out" : "source-over"
                            }
                        />
                    ))}
                </Layer>

            </Stage>
        </div>
    );
};

export default Pen;
