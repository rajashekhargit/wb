
import React, { useState,useEffect} from "react";
import Konva from "konva";
import { Group, Image, Rect,Stage,Layer } from "react-konva";
const Mp4 = () =>{
    let getNode;
    let animation;

    const video = document.createElement("video");
    const [link,setLink]=useState('https://upload.wikimedia.org/wikipedia/commons/transcoded/c/c4/Physicsworks.ogv/Physicsworks.ogv.240p.vp9.webm')
useEffect(()=>{
    video.addEventListener(
        "loadedmetadata",
        console.log("")
    );
    video.src = link;
    if(video){
        animation = new Konva.Animation(() => {},getNode.getLayer());
        console.log(animation)
    }
},[]);
   const play =() => {
        video.play().then(r => console.log(r));
       animation.start();
    };

   const pause=()=> {
        video.pause();
        animation.stop();
    };

   return (
       <div>
           <button onClick={play}>Play</button>
           <button onClick={pause}>Pause</button>
           <input onChange={e=>{
               setLink(e.target.value);
               console.log(e.target.value)
               video.addEventListener(
                   "loadedmetadata",
                   console.log("")
               );
               video.src = link;
               // if(video){
               animation = new Konva.Animation(() => {},getNode.getLayer());
               console.log(video)

           }}/>
           <Stage width={window.innerWidth - 20} height={window.innerHeight - 50}>
               <Layer>

                   <Image
                       draggable={true}
                       x={10}
                       y={10}
                       width={300}
                       height={300}
                       image={video}
                       ref={node => {
                           getNode = node;
                       }}
                   />
               </Layer>
           </Stage>

       </div>
   )


};

export default Mp4;
