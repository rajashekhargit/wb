import React from "react";
import { render } from "react-dom";
import { Stage, Layer, Line,Image } from "react-konva";
import { CirclePicker} from 'react-color';
import Konva from "konva";
import GooglePicker from "react-google-picker";
// import gapi from 'gapi'
import {gapi} from 'gapi-script'
const Upload =() => {
    const [layer, setLayer] = React.useState([]);
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const[size,setSize] = React.useState(3);
    const[color,setColor]= React.useState("red");
    const[image,setImage] = React.useState([]);
    const style = {
        backgroundColor:'white',
        cursor : 'crosshair'
    };
    let developerKey = 'AIzaSyAWKiW0bm9losRRfYuabxGAzdfH3FBN8KQ';
    let clientId = "711519097060-a2tu4q4oltdi9p60qoog3fkn8ac30nq5.apps.googleusercontent.com"
    let appId = "711519097060";
    let scope = ['https://www.googleapis.com/auth/drive.file'];
    let pickerApiLoaded = false;
    let oauthToken;


    const handleImage=(e) =>{
       let img;
       let URL = window.webkitURL || window.URL;
       console.log(e.target.files[0])
        let url = URL.createObjectURL(e.target.files[0]);
         img = new window.Image();
        img.src = url;
        console.log(img);
        img.onload=()=>{
            setImage([...image, img])
        }
        let x = document.getElementsByTagName('canvas')[0];
        console.log(x)

   };
    const newImage = (img) => ({
        image:img,
        x:150,
        y:200
    });
    return(

        <div>
            <CirclePicker  value={color} color={color} onChange = {e=>{
                setColor(e.hex);
            }}/>

            <select
                value={tool}
                onChange={e => {
                    setTool(e.target.value);
                }}
            >
                <option value="pen">Pen</option>
                <option value="eraser">Eraser</option>
            </select>
            <input type="file" onChange={handleImage}/>
            <input value={size} onChange={e =>{
                setSize(parseInt(e.target.value))
                console.log(setSize)
            }} type='range' step='3' min='3' max='16'/>
            <GooglePicker clientId={clientId}

                          developerKey={developerKey}
                          scope={['https://www.googleapis.com/auth/drive.readonly']}
                          onChange={data => console.log('on change:', data)}
                          onAuthFailed={data => console.log('on auth failed:', data)}
                          multiselect={true}
                          navHidden={true}
                          authImmediate={false}
                          viewId={'FOLDERS'}
                          createPicker={ (google, oauthToken) => {
                              const googleViewId = google.picker.ViewId.FOLDERS;
                              const docsView = new google.picker.DocsView(googleViewId)
                                  .setIncludeFolders(true)
                                  .setMimeTypes("image/png,image/jpeg,image/jpg")
                                  .setSelectFolderEnabled(true);

                              const picker = new window.google.picker.PickerBuilder()
                                  .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                                  .addView(docsView)
                                  .setOAuthToken(oauthToken)
                                  .setDeveloperKey(developerKey)
                                  .setCallback((data)=>{
                                      console.log('Custom picker is ready!',data.docs);
                                      if(data.docs){
                                          let blob;
                                          for(let i = 0; i<data.docs.length;i++){
                                              let obj = data.docs[i];
                                              var accessToken = gapi.auth.getToken().access_token;
                                              console.log(gapi)
                                              console.log(accessToken)
                                              var xhr = new XMLHttpRequest();
                                              var url = "https://www.googleapis.com/drive/v3/files/" + obj.id + "?alt=media";
                                              xhr.open('GET', url);
                                              xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
                                              xhr.responseType = "blob";

                                              xhr.addEventListener('load', function(e) {
                                                  blob = this.response;//this is your blob file
                                                  var file = new File([blob], "file_name", {lastModified: Date.now()});
                                                  console.log(file)
                                                  let img = new window.Image();
                                                  let url = URL.createObjectURL(file);
                                                  img.src = url;
                                                  img.onload=()=>{
                                                      setImage([...image, img])}
                                              });

                                              xhr.send();
                                          }
                                      }
                                  });

                              picker.build().setVisible(true);
                          }}
            >
                <button>Upload from Google drive</button>
                <div className="google"/>
            </GooglePicker>

            <Stage style={style}
                   width={window.innerWidth}
                   height={window.innerHeight}
                   onMouseDown={e => {
                       toggleDrawing(true)
                       const pointer = e.target.getStage().getPointerPosition();
                       const newLines = lines.concat({
                           id: Date.now(),
                           tool: tool,
                           points: [pointer.x, pointer.y]
                       });
                       setLines(newLines);
                   }}
                   onMouseMove={e => {
                       if (isDrawing) {
                           const pointer = e.target.getStage().getPointerPosition();
                           const newLines = lines.slice();
                           const lastLine = {
                               ...newLines[newLines.length - 1]
                           };
                           lastLine.size=size;
                           lastLine.color=color;
                           lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                           newLines[newLines.length - 1] = lastLine;
                           setLines(newLines);

                       }else{
                           return;
                       }

                   }}
                   onMouseUp={e => {
                       toggleDrawing(false);
                   }}
            >
<Layer id="hello">
    {image.map((image, key) => (
        <Image image={image} key={key} height={400} width={400} x={190} y={240} draggable={true}/>

    ),
        )}

</Layer>
                <Layer
                        clockwise={false}
                        // draggable
                        fill="blue"
                        onDragEnd={e => {
                            e.target.to({
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                scaleX: 1,
                                scaleY: 1,
                                shadowOffsetX: 0,
                                shadowOffsetY: 0
                            });
                        }}
                        onDragStart={e => {
                            e.target.setAttrs({
                                shadowOffset: {
                                    x: 15,
                                    y: 15
                                },
                                scaleX: 1.1,
                                scaleY: 1.1
                            });
                        }}
                    >
                        {lines.map(line => (
                            <Line
                                x={window.length}
                                y={window.length}
                                width={window.length}
                                height={window.length}
                                key={line.id}
                                strokeWidth={line.size}
                                stroke={line.color}
                                points={line.points}
                                globalCompositeOperation={
                                    line.tool === "eraser" ? "destination-out" : "source-over"
                                }
                            />
                        ))}
                    </Layer>
            </Stage>
                <CirclePicker  value={color} color={color} onChange = {e=>{
                    setColor(e.hex);
                }}/>

                <select
                    value={tool}
                    onChange={e => {
                        setTool(e.target.value);
                    }}
                >
                    <option value="pen">Pen</option>
                    <option value="eraser">Eraser</option>
                </select>
                <input type="file" onChange={handleImage}/>
                <input value={size} onChange={e =>{
                    setSize(parseInt(e.target.value))
                    console.log(setSize)
                }} type='range' step='3' min='3' max='16'/>
                <GooglePicker clientId={clientId}

                              developerKey={developerKey}
                              scope={['https://www.googleapis.com/auth/drive.readonly']}
                              onChange={data => console.log('on change:', data)}
                              onAuthFailed={data => console.log('on auth failed:', data)}
                              multiselect={true}
                              navHidden={true}
                              authImmediate={false}
                              viewId={'FOLDERS'}
                              createPicker={ (google, oauthToken) => {
                                  const googleViewId = google.picker.ViewId.FOLDERS;
                                  const docsView = new google.picker.DocsView(googleViewId)
                                      .setIncludeFolders(true)
                                      .setMimeTypes("image/png,image/jpeg,image/jpg")
                                      .setSelectFolderEnabled(true);

                                  const picker = new window.google.picker.PickerBuilder()
                                      .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                                      .addView(docsView)
                                      .setOAuthToken(oauthToken)
                                      .setDeveloperKey(developerKey)
                                      .setCallback((data)=>{
                                          console.log('Custom picker is ready!',data.docs);
                                          if(data.docs){
                                              let blob;
                                              for(let i = 0; i<data.docs.length;i++){
                                                  let obj = data.docs[i];
                                                  var accessToken = gapi.auth.getToken().access_token;
                                                  console.log(gapi)
                                                  console.log(accessToken)
                                                  var xhr = new XMLHttpRequest();
                                                  var url = "https://www.googleapis.com/drive/v3/files/" + obj.id + "?alt=media";
                                                  xhr.open('GET', url);
                                                  xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken);
                                                  xhr.responseType = "blob";

                                                  xhr.addEventListener('load', function(e) {
                                                      blob = this.response;//this is your blob file
                                                      var file = new File([blob], "file_name", {lastModified: Date.now()});
                                                      console.log(file)
                                                      let img = new window.Image();
                                                      let url = URL.createObjectURL(file);
                                                      img.src = url;
                                                      img.onload=()=>{
                                                          setImage([...image, img])}
                                                  });

                                                  xhr.send();
                                              }
                                          }
                                      });

                                  picker.build().setVisible(true);
                              }}
                >
                    <button>Upload from Google drive</button>
                    <div className="google"/>
                </GooglePicker>

                <Stage style={style}
                       width={window.innerWidth}
                       height={window.innerHeight}
                       onMouseDown={e => {
                           toggleDrawing(true)
                           const pointer = e.target.getStage().getPointerPosition();
                           const newLines = lines.concat({
                               id: Date.now(),
                               tool: tool,
                               points: [pointer.x, pointer.y]
                           });
                           setLines(newLines);
                       }}
                       onMouseMove={e => {
                           if (isDrawing) {
                               const pointer = e.target.getStage().getPointerPosition();
                               const newLines = lines.slice();
                               const lastLine = {
                                   ...newLines[newLines.length - 1]
                               };
                               lastLine.size=size;
                               lastLine.color=color;
                               lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                               newLines[newLines.length - 1] = lastLine;
                               setLines(newLines);

                           }else{
                               return;
                           }

                       }}
                       onMouseUp={e => {
                           toggleDrawing(false);
                       }}
                >
                    <Layer id="hello">
                        {image.map((image, key) => (
                                <Image image={image} key={key} height={400} width={400} x={190} y={240} draggable={true}/>

                            ),
                        )}

                    </Layer>
                    <Layer
                        clockwise={false}
                        // draggable
                        fill="blue"
                        onDragEnd={e => {
                            e.target.to({
                                duration: 0.5,
                                easing: Konva.Easings.ElasticEaseOut,
                                scaleX: 1,
                                scaleY: 1,
                                shadowOffsetX: 0,
                                shadowOffsetY: 0
                            });
                        }}
                        onDragStart={e => {
                            e.target.setAttrs({
                                shadowOffset: {
                                    x: 15,
                                    y: 15
                                },
                                scaleX: 1.1,
                                scaleY: 1.1
                            });
                        }}
                    >
                        {lines.map(line => (
                            <Line
                                x={window.length}
                                y={window.length}
                                width={window.length}
                                height={window.length}
                                key={line.id}
                                strokeWidth={line.size}
                                stroke={line.color}
                                points={line.points}
                                globalCompositeOperation={
                                    line.tool === "eraser" ? "destination-out" : "source-over"
                                }
                            />
                        ))}
                    </Layer>
                </Stage>
            </div>

    );
};

export default Upload;
