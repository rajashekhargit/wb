import React, { useState,useEffect} from "react";
import Konva from "konva";
import { Group, Image,Stage,Layer } from "react-konva";
const Avi = () =>{
    const[videos,setVideo] = useState([]);
    let getNode;
    let video = document.createElement("video");
    let animation;
    let canvas;
    let ctx;
    const handleChange = (e,play) =>{
        let URL = window.webkitURL || window.URL;
        let url = URL.createObjectURL(e.target.files[0]);
        video.addEventListener(
            "loadedmetadata",
            console.log(url)
        );
        // video.src = url;
        // console.log(video)
        setVideo([...videos,url]);
        console.log(videos)
        if(videos.length && !animation){
            // animation = new Konva.Animation(() => {},getNode.getLayer());

        }
    };
    const play =() => {
        video.play();
            animation.start();

    };

    const pause=()=> {
        video.pause();
        animation.stop();
    };


    return (
        <div>
            <button onClick={play}>Play</button>
            <button onClick={pause}>Pause</button>
            <input type="file" onChange={handleChange} accept="*"/>
            <Stage width={window.innerWidth - 20} height={window.innerHeight - 50}>
                <Layer>
                        {videos.map((vid,key)=>(
                            video.src = vid,
                            <Image
                            draggable={true}
                            x={10}
                            y={10}
                            key={key}
                            width={500}
                            height={500}
                            image={video}
                            ref={node => {
                            getNode = node;

                                if(!animation){
                                animation = new Konva.Animation(() => {},getNode.getLayer());
                            }
                            }}
                            />
                        ))}


                </Layer>
            </Stage>

        </div>
    )


};

export default Avi;
