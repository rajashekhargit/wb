import React from 'react';
import { render } from 'react-dom';
import { Stage, Layer, Rect, Transformer } from 'react-konva';

const Rectangle = ({ shapeProps, isSelected, onSelect, onChange,onColorChange }) => {
    const shapeRef = React.useRef();
    const trRef = React.useRef();
    const[fill,setFill]= React.useState("white");

    React.useEffect(() => {
        if (isSelected) {
            // we need to attach transformer manually
            trRef.current.setNode(shapeRef.current);
            trRef.current.getLayer().batchDraw();
        }
    }, [isSelected]);

    return (
        <React.Fragment>
            <Rect
                onDblClick={onColorChange}
                onDblTap={e=>{
                    shapeProps.fill("red")
                }}
                onClick={onSelect}
                ref={shapeRef}
                fill={fill}
                {...shapeProps}
                draggable
                onDragEnd={e => {
                    onChange({
                        ...shapeProps,
                        x: e.target.x(),
                        y: e.target.y()
                    });
                }}
                onTransformEnd={e => {

                    const node = shapeRef.current;
                    const scaleX = node.scaleX();
                    const scaleY = node.scaleY();

                    node.scaleX(1);
                    node.scaleY(1);
                    onChange({
                        ...shapeProps,
                        x: node.x(),
                        y: node.y(),
                        width: Math.max(5, node.width() * scaleX),
                        height: Math.max(node.height() * scaleY)
                    });
                }}
            />
            {isSelected && (
                <Transformer
                    ref={trRef}
                    boundBoxFunc={(oldBox, newBox) => {
                        // limit resize
                        if (newBox.width < 5 || newBox.height < 5) {
                            return oldBox;
                        }
                        return newBox;
                    }}
                />
            )}
        </React.Fragment>
    );
};

const initialRectangles = [
    {
        x: 10,
        y: 10,
        width: 100,
        height: 100,
        fill: 'red',
        id: 'rect1'
    },
    {
        x: 150,
        y: 150,
        width: 100,
        height: 100,
        fill: 'green',
        id: 'rect2'
    }
];





const Recti = () => {
    const [rectangles, setRectangles] = React.useState([]);
    const [selectedId, selectShape] = React.useState(null);
    const [x, setX] = React.useState(250);
    const [y, setY] = React.useState(300);
    const [width, setWidth] = React.useState(100);
    const [height, setHeight] = React.useState(100);
    const [angle, setAngle] = React.useState(100);
    const[fill,setFill]= React.useState("white");
    const [radius, setRadius] = React.useState(70);

    const newRectangle = () => ({
        x: (x),
        y: (y),
        width: (width),
        height: (height),
        fill:fill,
        stroke:'black',
        angle:angle,
        id:"Rect" + Date.now()
    });
    const newCircle = () =>({
        x: 200,
        y:100,
        radius:radius
    });
    const handleRect = () => {

        setRectangles( [...rectangles, { ...newRectangle() }])
    };
    const checkDeselect = e => {
        const clickedOnEmpty = e.target === e.target.getStage();
        if (clickedOnEmpty) {
            selectShape(null);
        }
    };

    return (

      <div>
          <button onClick={handleRect}>Add Rect</button>
        <Stage
            width={window.innerWidth}
            height={window.innerHeight}
            onMouseDown={checkDeselect}
            onTouchStart={checkDeselect}
        >
            <Layer>
                {rectangles.map((rect, i) => {
                    return (
                        <Rectangle
                            key={i}
                            shapeProps={rect}
                            isSelected={rect.id === selectedId}
                            onSelect={() => {
                                selectShape(rect.id);
                            }}
                            onColorChange={e=>{
                                setFill("red")
                            }}
                            onChange={newAttrs => {
                                const rects = rectangles.slice();
                                rects[i] = newAttrs;
                                setRectangles(rects);
                            }}
                        />
                    );
                })}
            </Layer>
        </Stage>
      </div>
    );
};

export default Recti

