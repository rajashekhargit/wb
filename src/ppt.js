import React from "react";
import Convert from 'ppt-png'

const Ppt = () =>{

        const [src,setSrc] = React.useState('')

    const handleChange = (e) =>{
    let file = e.target.files[0];
    let url =window.webkitURL || window.url;
    let u = url.createObjectURL(file)
        setSrc(u);
    let convert = new Convert({
        files : [file],
        output : 'src/',
        invert:   true,
        callback: function(data) {
            console.log(data);
        }

    }).run();
    };


    return(
        <div>
            <input type="file" onChange={handleChange}/>
            <iframe src={src}
                     frameBorder="0"></iframe>

        </div>
    )
}


export default Ppt;
