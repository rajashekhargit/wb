import React from "react";
import { render } from "react-dom";
import { Stage, Layer, Line } from "react-konva";
import { CirclePicker} from 'react-color';
import Konva from "konva";

const Move =() => {
    const [layer, setLayer] = React.useState([]);
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const[size,setSize] = React.useState(3);
    const[color,setColor]= React.useState("red");
    const style = {
        backgroundColor:'white',
        cursor : 'crosshair'
    };
    const handleLayer = () => {
        setLayer([...layer, {...newLayer()}])
    };
    const handleErase = () => {
        toggleDrawing(false)
    }
    const newLayer = () => ({
        width: window.innerWidth,
        height: window.innerHeight,
        fill: 'black'
    });
return(

    <div>
        <CirclePicker  value={color} color={color} onChange = {e=>{
            setColor(e.hex);
        }}/>

        <select
            value={tool}
            onChange={e => {
                setTool(e.target.value);
            }}
        >
            <option value="pen">Pen</option>
            <option value="eraser">Eraser</option>
        </select>
        <input value={size} onChange={e =>{
            setSize(parseInt(e.target.value))
            console.log(setSize)
        }} type='range' step='3' min='3' max='16'/>

        <button onClick={handleLayer}>Add pen</button>
        <button onClick={handleErase}>Add erase</button>

        <Stage style={style}

               width={window.innerWidth}
               height={window.innerHeight}
               onMouseDown={e => {
                   toggleDrawing(true)
                       const pointer = e.target.getStage().getPointerPosition();
                       const newLines = lines.concat({
                           id: Date.now(),
                           tool: tool,
                           points: [pointer.x, pointer.y]
                       });
                       setLines(newLines);
               }}
               onMouseMove={e => {
                   if (isDrawing) {
                       const pointer = e.target.getStage().getPointerPosition();
                       const newLines = lines.slice();
                       const lastLine = {
                           ...newLines[newLines.length - 1]
                       };
                       lastLine.size=size;
                       lastLine.color=color;
                       lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                       newLines[newLines.length - 1] = lastLine;
                       setLines(newLines);

                   }else{
                   return;
               }

               }}
               onMouseUp={e => {
                   toggleDrawing(false);
               }}
        >

            {layer.map(({  angle, innerRadius,outerRadius,x,y }, key) => ( // like a "for loop", this maps over this.state.canvas objects and pulls out the height, width, x, y properties to be used below
                <Layer
                    key={key}
                    angle={angle}
                    innerRadius={innerRadius}
                    outerRadius={outerRadius}
                    clockwise={false}
                    draggable
                    fill="blue"
                    onDragEnd={e => {
                        e.target.to({
                            duration: 0.5,
                            easing: Konva.Easings.ElasticEaseOut,
                            scaleX: 1,
                            scaleY: 1,
                            shadowOffsetX: 0,
                            shadowOffsetY: 0
                        });
                    }}
                    onDragStart={e => {
                        e.target.setAttrs({
                            shadowOffset: {
                                x: 15,
                                y: 15
                            },
                            scaleX: 1.1,
                            scaleY: 1.1
                        });
                    }}
                >
                    {lines.map(line => (
                        <Line
                            x={window.length}
                            y={window.length}
                            width={window.length}
                            height={window.length}
                            key={line.id}
                            strokeWidth={line.size}
                            stroke={line.color}
                            points={line.points}
                            globalCompositeOperation={
                                line.tool === "eraser" ? "destination-out" : "source-over"
                            }
                        />
                    ))}
                </Layer>
            ))}
        </Stage>
    </div>
);
};

export default Move;


