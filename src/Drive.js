import React from "react";
import GooglePicker from 'react-google-picker'

const Drive = () =>{

    let developerKey = 'AIzaSyAWKiW0bm9losRRfYuabxGAzdfH3FBN8KQ';
    let clientId = "711519097060-a2tu4q4oltdi9p60qoog3fkn8ac30nq5.apps.googleusercontent.com"
    let appId = "711519097060";
    let scope = ['https://www.googleapis.com/auth/drive.file'];
    let pickerApiLoaded = false;
    let oauthToken;





    return(
        <div>
            <h1>Hello Google Drive</h1>
            <GooglePicker clientId={clientId}
                          developerKey={developerKey}
                          scope={['https://www.googleapis.com/auth/drive.readonly']}
                          onChange={data => console.log('on change:', data)}
                          onAuthFailed={data => console.log('on auth failed:', data)}
                          multiselect={true}
                          navHidden={true}
                          authImmediate={false}
                          viewId={'FOLDERS'}
                          createPicker={ (google, oauthToken) => {
                              const googleViewId = google.picker.ViewId.FOLDERS;
                              const docsView = new google.picker.DocsView(googleViewId)
                                  .setIncludeFolders(true)
                                  .setMimeTypes("image/png,image/jpeg,image/jpg")
                                  .setSelectFolderEnabled(true);

                              const picker = new window.google.picker.PickerBuilder()
                                  .addView(docsView)
                                  .setOAuthToken(oauthToken)
                                  .setDeveloperKey(developerKey)
                                  .setCallback(()=>{
                                      console.log('Custom picker is ready!');
                                  });

                              picker.build().setVisible(true);
                          }}
            >
                <span>Click</span>
                <div className="google"/>
            </GooglePicker>
        </div>
    )
};

export default Drive;
