import React from "react";
import Konva from 'konva';

class Draw extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            mode: 'brush'
        };
        this.isPaint = false;
         this.mode = 'brush';
         this.lastLine =null;
         this.stage='';
         this.style ={
             backgroundColor: 'black'
        }
    };
   componentDidMount() {

       let stage = new Konva.Stage({
           container: 'container',
           width: 1000,
           height: 1000,
       });
       console.log(stage)
    let layer = new Konva.Layer();
       stage.add(layer);
       this.isPaint = false;
       this.mode = this.state.mode;
       this.lastLine =null;
       stage.on('mousedown touchstart', function (e) {
           this.isPaint = true;
           var pos = stage.getPointerPosition();
           this.lastLine = new Konva.Line({
               stroke: '#df23d1',
               strokeWidth: 5,
               globalCompositeOperation: 'source-over',
               points: [pos.x, pos.y],
           });
           layer.add(this.lastLine);
       });
       stage.on('mouseup touchend', function () {
           this.isPaint = false;
       });

       // and core function - drawing
       stage.on('mousemove touchmove', function () {
           if (!this.isPaint) {
               return;
           }

           const pos = stage.getPointerPosition();
           var newPoints = this.lastLine.points().concat([pos.x, pos.y]);
           this.lastLine.points(newPoints);
           layer.batchDraw();
       });
   };
   componentDidUpdate(prevProps, prevState, snapshot) {

   }

    eraser = (ev) =>{
        if(ev.target.value === 'eraser'){
            console.log('eraser')

        }
        this.setState({
            mode:ev.target.value
        })
    };
    render() {
        return(
            <div>
                <h1>Hello Drawing</h1>
                <select onChange={this.eraser}>
                    <option value='brush'>Brush</option>
                    <option value='eraser' >Eraser</option>
                </select>
                <div style={this.style} id="container"></div>
            </div>
        )
    }

}


export default Draw;
