import React from "react";
import {Layer,Stage,Text,Image, Group} from "react-konva";
import Konva from "konva";



const NewPdf = () =>{
    let Multiple=[];

    const[image,setImage] = React.useState([]);
    const [stageScale,setstageScale] = React.useState(1);
    const [stageX,setstageX] =React.useState(0);
    const [stageY,setstageY] =React.useState(0);
    const style = {
        backgroundColor:'white',
        cursor : 'crosshair'
    };
    let img;

    const imgStyle={
        float:"left",
        marginRight: 5,
    }
  const  handleChange = (e) => {

          let file = e.target.files[0];
          // console.log(file)
          if (file.type !== "application/pdf") {
              console.error(file.name, "is not a pdf file.")
              return
          }
      let PDFJS = window['pdfjs-dist/build/pdf'];

          let fileReader = new FileReader();

          fileReader.onload = function () {
              // console.log(fileReader.result);

              let typedarray = new Uint8Array(fileReader.result);

              PDFJS.getDocument(typedarray).then(function (pdf) {
                  // pdf.getPage(2);
// console.log(pages)
                  console.log("the pdf has ", pdf.numPages, "page(s).");
                for(let i=0;i<pdf.numPages;i++) {
                    console.log(i)
                    if (pdf.numPages.length <= 1) {

                    pdf.getPage(i + 1).then(function (page) {
                        let viewport = page.getViewport(2);
                        let canvasEl = document.getElementsByTagName("canvas")[0]
                        // console.log(canvasEl);
                        // console.log(pdf);
                        canvasEl.height = viewport.height;
                        canvasEl.width = viewport.width;

                        page.render({
                            canvasContext: canvasEl.getContext('2d'),
                            viewport: viewport,
                            // background: 'rgba(0,0,0,0)',

                        }).then(function () {

                            var bg = canvasEl.toDataURL("image/png");
                            // console.log(bg)


                            let URL = window.webkitURL || window.URL;
                            // let url = URL.createObjectURL('');
                            // let  img = document.createElement('img');
                            // document.body.appendChild(img);
                            // img.src=bg
                            // console.log(img)
                            img = new window.Image();
                            img.src = bg;
                            img.width = 200;
                            img.height = 300;
                            img.background = 'rgba(0,0,0,0)';
                            setImage([...image, img])


                            // img.scaleToHeight(1123);
                            // canvasEl.height = (1123);
                            // canvasEl.width = (1588);
                            // canvasEl.setBackgroundImage(img);
                        });
                    });
                }else{
                        pdf.getPage(i + 1).then(function (page) {
                            let viewport = page.getViewport(2);
                            let canvasEl = document.getElementsByTagName("canvas")[0]
                            // console.log(canvasEl);
                            // console.log(pdf);
                            canvasEl.height = viewport.height;
                            canvasEl.width = viewport.width;

                            page.render({
                                canvasContext: canvasEl.getContext('2d'),
                                viewport: viewport,
                                // background: 'rgba(0,0,0,0)',

                            }).then(function () {

                                var bg = canvasEl.toDataURL("image/png");
                                // console.log(bg)


                                let URL = window.webkitURL || window.URL;
                                // let url = URL.createObjectURL('');
                                // let  img = document.createElement('img');
                                // document.body.appendChild(img);
                                // img.src=bg
                                // console.log(img)
                                img = new window.Image();
                                img.src = bg;
                                img.width = 200;
                                img.height = 300;
                                img.background = 'rgba(0,0,0,0)';
                                Multiple.push(img);
                                // if(pdf.numPages.length=== i+1){
                                    // if(Multiple.length){
                                        setImage([...image, img]);
                                        console.log(image);
                                    // }

                                // }


                                // img.scaleToHeight(1123);
                                canvasEl.height = (1123);
                                canvasEl.width = (1588);
                                // canvasEl.setBackgroundImage(img);
                            });
                        });
                    }
                    // if((1+i) === pdf.numPages){
                        // console.log("Hey Dude")

                    // }
                }


              });
          };
          fileReader.readAsArrayBuffer(file);
  };
  const  handleWheel = e => {
        e.evt.preventDefault();

        const scaleBy = 1.01;
        const stage = e.target.getStage();
        const oldScale = stage.scaleX();
        const mousePointTo = {
            x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
            y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
        };

        const newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

        stage.scale({ x: newScale, y: newScale });
setstageScale(newScale);
setstageX( -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale);
setstageY(-(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale);

    };
    return(
        <div>
            <input type="file" accept=".pdf" onChange={handleChange}/>

            <Stage
                width={window.innerWidth}
                height={window.innerHeight}
                scaleX={stageScale}
                scaleY={stageScale}
                x={stageX}
                y={stageY}
            >
                <Layer >
                    {image.map((img,key) =>
                        (!Array.isArray(img)) ?
                            (
                        <Group
                            onWheel={handleWheel}
                            x={100 +(key*200)} y={100 }
                            draggable={true}
                            key={key}

                        >
                        <Image
                            style={imgStyle}

                            // key={key}
                              width={400} height={600} image={img}/>
                        </Group> ):(
                            <Group
                                onWheel={handleWheel}
                            >
                                {img.map((img,key)=>(

                                    <Image draggable={true}
                                        style={imgStyle}
                                           x={100 +(key + 100)} y={100 }
                                        key={key}
                                           width={400} height={600} image={img}/>
                                ))}
                            </Group>)
                    )}

                </Layer>
            </Stage>

        </div>
    )
}
export  default  NewPdf;
