import React from "react"
import Konva from "konva";
import {Group, Layer, Line, Stage,Transformer} from "react-konva";
import {CirclePicker} from "react-color";
const Pencil =() => {
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const[size,setSize] = React.useState(3);
    const[color,setColor]= React.useState("red");
    const [stageScale,setstageScale] = React.useState(1);
    const [stageX,setstageX] =React.useState(0);
    const [stageY,setstageY] =React.useState(0);
    const [stageDrag,setStageDrag] = React.useState(false);
    const [mouseDown,setMouseDown] = React.useState(false);
    const [mouseUp,setMouseUp] = React.useState(false);
    const [mouseMove,setMouseMove] = React.useState(false);
    let scaleBy = 1.3;
    let stage;

    const endHand =() => {
setStageDrag(false)
    };
    const getRelativePointerPosition = (node) => {
        // the function will return pointer position relative to the passed node
        var transform = node.getAbsoluteTransform().copy();
        // to detect relative position we need to invert transform
        transform.invert();

        var pos = node.getStage().getPointerPosition();

        // now we find relative point
        return transform.point(pos);
    };
    const usePen =()=> {
      toggleDrawing(false)
    };

    const endPen =  () => {
       setMouseDown(false);
       setMouseMove(false);
       setMouseUp(false);
    }
        return(
    <div>
        <CirclePicker  value={color} color={color} onChange = {e=>{
            setColor(e.hex);
        }}/>
        <select
            value={tool}
            onChange={e => {
                let mode = e.target.value;
                setTool(e.target.value);

                switch (mode) {

                    case 'hand' :{
                        setStageDrag(true);
                        toggleDrawing(false);
                        break;
                    }
                    case 'pen':{
                        toggleDrawing(false);
                        setStageDrag(false);

                        break;
                    }

                }
            }}
        >
            <option value="pen">Pen</option>
            <option value="eraser">Eraser</option>
            <option value="hand">Hand</option>
        </select>
        <input value={size} onChange={e =>{
            setSize(parseInt(e.target.value))
            console.log(setSize)
        }} type='range' step='3' min='3' max='16'/>
        <input  onChange={e=>{
            setSize(parseInt(e.target.value))
            console.log(size);
        }
        }/>
    <Stage
        onWheel={e=>{
            if(!stageDrag){
                return;
            }
            e.evt.preventDefault();
            // let oldScale = e.target.getStage().getPointerPosition().scaleX;
            let oldScale = e.target.getStage().scaleX();
            console.log(oldScale);

            let mousePointTo = {
                x: e.target.getStage().getPointerPosition().x / oldScale - e.target.getStage().x() / oldScale,
                y: e.target.getStage().getPointerPosition().y / oldScale - e.target.getStage().y() / oldScale
            };
            console.log(mousePointTo)

            let newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;
            // stage.scale({ x: newScale, y: newScale });
            console.log("old/: " + oldScale);
            console.log("new/: " + newScale)
            setstageScale(newScale);

            let newPos = {
                x: -(mousePointTo.x - e.target.getStage().getPointerPosition().x / newScale) * newScale,
                y: -(mousePointTo.y - e.target.getStage().getPointerPosition().y / newScale) * newScale
            };
                let sxe= newPos.x + newPos.y;
                console.log("x" + newPos.x);
                console.log("y" + newPos.y)
            console.log("total: " +newPos.x + newPos.y);
            if(newScale <0.0004){
                setSize(sxe*8)
            }else{
                setSize(sxe/7);
            }


            // stage.position(newPos);
            setstageY(newPos.y);
            setstageX(newPos.x);
            // stage.batchDraw();

        }}
        draggable={stageDrag}
        width={window.innerWidth}
        height={window.innerHeight}
        scaleX={stageScale}
        scaleY={stageScale}
        x={stageX}
        y={stageY}
        onMouseDown={e => {
            if(stageDrag){
                return;
            }
            toggleDrawing(true);
            let pointer = getRelativePointerPosition(e.target.getStage());
            const newLines = lines.concat({
                id: Date.now(),
                tool: tool,
                points: [pointer.x, pointer.y]
            });
            setLines(newLines);
        }}
        onMouseMove={e => {
            if (!isDrawing) {
                return;
            }
            // if(stageScale >8){
            //     setSize(0.1)
            // }
            console.log(stageX,stageY,stageScale)
            // setstageScale(1)
            let pointer = getRelativePointerPosition(e.target.getStage());
            const newLines = lines.slice();
            const lastLine = {
                ...newLines[newLines.length - 1]
            };


            lastLine.size=size;
            console.log(size)
            lastLine.color=color;
            lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
            newLines[newLines.length - 1] = lastLine;
            // console.log(newLines)
            setLines(newLines);
        }}
        onMouseUp={e => {
            toggleDrawing(false);
        }}>
    <Layer
    >
        {lines.map(line => (
            <Line
                // draggable={true}
                x={window.length}
                y={window.length}
                width={window.length}
                height={window.length}

                key={line.id}
                strokeWidth={line.size}
                stroke={line.color}
                points={line.points}
                globalCompositeOperation={
                    line.tool === "eraser" ? "destination-out" : "source-over"
                }
            />
        ))}
    </Layer>
    </Stage>
    </div>)



};


export default Pencil;
