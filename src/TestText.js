import Konva from "konva";
import {Stage,Layer,Text,Transformer,Rect,Circle} from "react-konva";
import React from "react";

const NewText = () =>{
    const [shape, setShape] = React.useState("");
    const[x,setX] = React.useState(0);
    const[transformer,setTransformer] = React.useState(null);
const [selectedNode,setSelectedNode] = React.useState(null);


    // let transformer;
    // let selectedNode;
    let stage;
    // let x=102;

    return(
    <div>

        <div>

            {shape ? <button onClick={e=>{
                selectedNode.visible(false)
                selectedNode.destroy();
                transformer.hide();
                setShape('');
            }}>Remove Selected</button> : null}

        </div>

        <Stage
            // onClick={e=>{
            //     setShape(e.target.attrs.name);
            //     console.log(shape)
            // }}
        height={window.outerHeight}
        width={window.outerWidth}

        >
            <Layer
                onDblClick={e=>{
                    // transformer.nodes(['rect1','rect2'])
                    setShape(e.target.attrs.name);

                    stage = transformer.getStage();
                    setSelectedNode (stage.findOne("." + e.target.attrs.name));
                    console.log(selectedNode);
                    transformer.show();
                    // console.log(transformer.getWidth());
                    // console.log(selectedNode.getWidth());
                    // setX(transformer.getWidth());

                    // setShape(e.target.attrs.name)
                    if (selectedNode === transformer.node()) {
                        return;
                    }
                    if (selectedNode) {
                        transformer.attachTo(selectedNode);

                    } else {
                        transformer.detach();
                    }
                    transformer.getLayer().batchDraw();
                }}
                >
                <Rect

                    height={100}
                    width={100}
                    x={200}
                    y={200}
                    stroke={2}
                    fill={"pink"}
                    name="rect1"
                    draggable={true}
                    // onClick={e=>{
                    //     transformer.destroy();
                    //
                    // }}
                />
                <Rect
                    draggable={true}
                    name="rect2"
                    height={100}
                    width={100}
                    x={500}
                    y={200}
                    stroke={2}
                    fill={"yellow"}

                />
                <Circle radius={100}
                        x={400}
                        y={200}
                        stroke={2}
                        fill={"yellow"}
                        // onClick={evt => {
                        //     if(setSelectedNode){
                        //         selectedNode.destroy()
                        //
                        //     }
                        // }}
                        name={"circle"}
                />
                <Circle

                    x={x}
                y= {0}
                radius= {10}
                fill= 'red'

                />
                <Transformer
                    ref={node => {
                        setTransformer(node);
                    }}/>
            </Layer>
        </Stage>

    </div>
)
};



export default NewText;
