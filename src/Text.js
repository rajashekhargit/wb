import React from "react"
import Konva from "konva";
import {Layer, Line, Text, Stage, Transformer, Ellipse} from "react-konva";
import {CirclePicker} from "react-color";
const Word =() => {
    const [tool, setTool] = React.useState("pen");
    const [isDrawing, toggleDrawing] = React.useState(false);
    const [lines, setLines] = React.useState([]);
    const [size, setSize] = React.useState(3);
    const [color, setColor] = React.useState("red");
    const [textValue, setTextValue] = React.useState('Hello');
    const [textEditVisible, setTextEditVisible] = React.useState(false);
    const [textX, setTextX] = React.useState(0);
    const [textY, setTextY] = React.useState(0);
    const [shape, setShape] = React.useState("");

const max=200;
const min=100;
    const newText = () => ({
        x: min + Math.random() * (max - min),
        y:  min + Math.random() * (max - min),
        width: (200),
        fill:'white',
        text:{textValue},
        fontSize:20
    });
    const handleText = () =>{
        setTexts([...texts,{...newText()}]);
    }
    let transformer;
    let selectedNode;
    let stage;
    const[texts,setTexts] = React.useState([])
    const handleTextDblClick = e => {
        const absPos = e.target.getAbsolutePosition();
        setTextEditVisible(true);
        setTextX(absPos.x);
        setTextY(absPos.y);
    };
    const handleTextEdit = e => {
        setTextValue(e.target.value);
    };
    const handleTextareaKeyDown = e => {
        if (e.keyCode === 13) {
            setTextEditVisible(false);
        }
    };
    const style = {
        border: "solid blue"
    };

    return(
        <div>
            <CirclePicker  value={color} color={color} onChange = {e=>{
                setColor(e.hex);
            }}/>
            <button onClick={handleText}>Add Text</button>
            <select
                value={tool}
                onChange={e => {
                    setTool(e.target.value);
                }}
            >
                <option value="pen">Pen</option>
                <option value="eraser">Eraser</option>
            </select>
            <Stage
                onClick={e=>{
                    setShape(e.target.attrs.name)
                }}
                width={window.innerWidth}
                height={window.innerHeight}
                onMouseDown={e => {
                    toggleDrawing(true);
                    const pointer = e.target.getStage().getPointerPosition();
                    const newLines = lines.concat({
                        id: Date.now(),
                        tool: tool,
                        points: [pointer.x, pointer.y]
                    });
                    setLines(newLines);
                }}
                onMouseMove={e => {
                    if (!isDrawing) {
                        return;
                    }
                    const pointer = e.target.getStage().getPointerPosition();
                    const newLines = lines.slice();
                    const lastLine = {
                        ...newLines[newLines.length - 1]
                    };
                    lastLine.size=size;
                    lastLine.color=color;
                    lastLine.points = lastLine.points.concat([pointer.x, pointer.y]);
                    newLines[newLines.length - 1] = lastLine;
                    setLines(newLines);
                }}
                onMouseUp={e => {
                    toggleDrawing(false);
                }}>
                <Layer>
                    {texts.map(({  width, x, y,fontSize }, key) => (

                    <Text
                        name={"text" + key}
                        key={key}
                        width= {200}
                        scaleX={1}
                        onClick={e=>{
                            stage = transformer.getStage();
                            selectedNode = stage.findOne("." + shape);
                            console.log(selectedNode);
                            transformer.show();
                            if (selectedNode === transformer.node()) {
                                return;
                            }
                            if (selectedNode) {
                                transformer.attachTo(selectedNode);
                                if(selectedNode.text() === "Type Text Here"){
                                    selectedNode.text('')
                                }

                            } else {
                                transformer.detach();
                            }
                            transformer.getLayer().batchDraw();
                        }}
                        draggable
                        text="Type Text Here"
                        x={x}
                        y={y}
                        fontSize={20}
                        onDblClick={e=>{
                            transformer.hide();
                            selectedNode.hide();
                            // stage.hide();
                            let textPosition = selectedNode.absolutePosition();
                            let stageBox = stage.container().getBoundingClientRect();
                            let areaPosition = {
                                x: stageBox.left + textPosition.x,
                                y: stageBox.top + textPosition.y,
                            };
                         let  textarea = document.createElement('textarea');
                            document.body.appendChild(textarea);
                            if(selectedNode.text() === "Type Text Here"){
                                selectedNode.text('')
                            }
                            if(selectedNode.text() !== "Type Text Here"){
                                transformer.detach(selectedNode);
                            }
                            textarea.value = selectedNode.text();
                            textarea.style.position = 'absolute';
                            textarea.style.top = areaPosition.y + 'px';
                            textarea.style.left = areaPosition.x + 'px';
                            textarea.style.width = selectedNode.width() - selectedNode.padding() * 2 + 'px';
                            textarea.style.height =
                                selectedNode.height() - selectedNode.padding() * 2 + 5 + 'px';
                            textarea.style.fontSize = selectedNode.fontSize() + 'px';
                            textarea.style.border = 'none';
                            textarea.style.padding = '0px';
                            textarea.style.margin = '0px';
                            textarea.style.overflow = 'hidden';
                            textarea.style.background = 'none';
                            textarea.style.outline = 'none';
                            textarea.style.resize = 'none';
                            textarea.style.lineHeight = selectedNode.lineHeight();
                            textarea.style.fontFamily = selectedNode.fontFamily();
                            textarea.style.transformOrigin = 'left top';
                            textarea.style.textAlign = selectedNode.align();
                            textarea.style.color = selectedNode.fill();
                            let px = 0;
                            let isFirefox =
                                navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
                            if (isFirefox) {
                                px += 2 + Math.round(selectedNode.fontSize() / 20);
                            }

                            textarea.style.transform = 'translateY(-' + px + 'px)';

                            textarea.style.height = 'auto';
                            textarea.style.height = textarea.scrollHeight + 3 + 'px';

                            textarea.focus();

                            textarea.addEventListener('keydown', function (e) {
                                let scale = selectedNode.getAbsoluteScale().x;
                                if(transformer){
                                    transformer.hide();
                                }
                                setTextareaWidth(selectedNode.width() * scale);
                                textarea.style.height = 'auto';
                                selectedNode.text(textarea.value);
                                textarea.style.height =
                                    textarea.scrollHeight + selectedNode.fontSize() + 'px';
                            });
                            setTimeout(() => {
                                window.addEventListener('click', handleOutsideClick);
                            });
                            selectedNode.on('transform', function () {
                                console.log("hello")
                                // reset scale, so only with is changing by transformer
                                selectedNode.setAttrs({
                                    width: selectedNode.width() * selectedNode.scaleX(),
                                    scaleX: 1,
                                });
                            });
                            function setTextareaWidth(newWidth) {
                                if (!newWidth) {
                                    selectedNode.text(textarea.value)
                                    // set width for placeholder
                                    newWidth = selectedNode.placeholder.length * selectedNode.fontSize();
                                }
                                // some extra fixes on different browsers
                                let isSafari = /^((?!chrome|android).)*safari/i.test(
                                    navigator.userAgent
                                );
                                let isFirefox =
                                    navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
                                if (isSafari || isFirefox) {
                                    newWidth = Math.ceil(newWidth);
                                }

                                let isEdge =
                                    document.documentMode || /Edge/.test(navigator.userAgent);
                                if (isEdge) {
                                    newWidth += 1;
                                }
                                textarea.style.width = newWidth + 'px';
                            }

                            function removeTextarea() {
                                textarea.parentNode.removeChild(textarea);
                                selectedNode.text(textarea.value);
                                window.removeEventListener('click', handleOutsideClick);
                                selectedNode.show();
                                // stage.show();
                                // transformer.hide();
                                // transformer.forceUpdate()
                            }
                            function handleOutsideClick(e) {
                                if (e.target !== textarea) {

                                    console.log(selectedNode);
                                    selectedNode.text(textarea.value);
                                    console.log(selectedNode.text());
                                    // transformer.hide();
                                    // transformer.forceUpdate()
                                    removeTextarea();
                                }
                            }
                        }}
                    />
                        ))}
                    <Transformer
                        rotateEnabled={false}
                        selectedShapeName={shape}
                        enabledAnchors="'middle-left', 'middle-right'"
                        ref={node => {
                            transformer = node;
                        }}/>
                </Layer>
                <Layer
                >
                    {lines.map(line => (
                        <Line
                            draggable={true}
                            x={window.length}
                            y={window.length}
                            width={window.length}
                            height={window.length}
                            onDragEnd={e => {
                                e.target.to({
                                    // duration: 0.5,
                                    // easing: Konva.Easings.ElasticEaseOut,
                                    // scaleX: 1,
                                    // scaleY: 1,
                                    shadowOffsetX: 0,
                                    shadowOffsetY: 0
                                });
                            }}
                            key={line.id}
                            strokeWidth={line.size}
                            stroke={line.color}
                            points={line.points}
                            globalCompositeOperation={
                                line.tool === "eraser" ? "destination-out" : "source-over"
                            }
                        />
                    ))}
                </Layer>

            </Stage>
        </div>)



};


export default Word
